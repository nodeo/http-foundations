<?php

/** @noinspection PhpDeprecationInspection */

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Tests;

use HTTP\URL\Url;
use HTTP\URL\UrlFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class UrlTest
 *
 * @package Tests
 * Tests of {@link Url}.
 */
class UrlTest extends TestCase
{
    public function testUri()
    {
        $uri = parse_url('n^odeo_test.fr/posts?id=1');

        if (isset($uri['http'])) {
            $t = $uri['http'];
        } else {
            $t = 'a';
        }

        self::assertEquals('a', $t);
    }

    public function testUriCreationWithGlobals()
    {
        $_SERVER['REQUEST_URI'] = 'https://nodeo.fr:443/posts?id=1';

        $uri = UrlFactory::createUrlFromGlobals();

        self::assertEquals('https', $uri->getScheme());
        self::assertEquals('nodeo.fr', $uri->getAuthority());
        self::assertEquals('/posts', $uri->getPath());
        self::assertNull($uri->getPort()); // Null since it is the default port
        self::assertEquals('?id=1', $uri->getQuery()->__toString());
    }
}
