<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace Tests;

use HTTP\AbstractMessage;
use IO\Stream\StreamFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class MessageTest
 * @package Tests
 *
 * Tests of {@link AbstractMessage}.
 */
class MessageTest extends TestCase
{
    public function testWithHeader()
    {
        $message = $this->getMockForAbstractClass(AbstractMessage::class, []);
        $message = $message->withHeader('Location', 'index.php');

        self::assertTrue($message->hasHeader('location'));

        self::assertEquals('index.php', $message->getHeaderLine('Location'));

        $message = $message->withHeader('location', 'nav.php');

        self::assertEquals('nav.php', $message->getHeaderLine('Location'));
        self::assertEquals('nav.php', $message->getHeader('Location')[0]);
    }

    public function testWithAddedHeader()
    {
        $message = $this->getMockForAbstractClass(AbstractMessage::class);

        $message = $message->withAddedHeader('Location', 'index.php');
        self::assertEquals('index.php', $message->getHeaderLine('Location'));

        $message = $message->withAddedHeader('Location', 'nav.php');
        self::assertEquals(
            'index.php,nav.php',
            $message->getHeaderLine('Location')
        );

        $message = $message->withHeader('location', 'registration.php');
        self::assertEquals(
            'registration.php',
            $message->getHeaderLine('Location')
        );
    }

    public function testWithoutHeader()
    {
        $message = $this->getMockForAbstractClass(AbstractMessage::class);

        $message = $message->withHeader('Location', 'index.php');
        $message = $message->withoutHeader('Location');

        self::assertEmpty($message->getHeaders());
    }

    public function testBody()
    {
        $message = $this->getMockForAbstractClass(AbstractMessage::class);
        $body = (new StreamFactory())->createStream('A test content');

        $message = $message->withBody($body);
        self::assertEquals($message->getBody(), $body);

        $body = (new StreamFactory())->createStream('A test content');
        self::assertNotEquals($message->withBody($body), $message);
    }

    public function testProtocolVersion()
    {
        $message = $this->getMockForAbstractClass(AbstractMessage::class);
        $message->withProtocolVersion('1.1');

        self::assertEquals('1.1', $message->getProtocolVersion());
    }
}
