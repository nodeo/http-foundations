<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP;

use IO\Stream\StreamInterface;
use JetBrains\PhpStorm\Pure;

/**
 * Class AbstractMessage
 *
 * This class is the default implementation of {@link MessageInterface}.
 *
 * @see https://tools.ietf.org/html/rfc7230
 * @package HTTP
 */
abstract class AbstractMessage implements MessageInterface
{
    /**
     * The message body.
     *
     * @var StreamInterface|null
     */
    protected ?StreamInterface $body;

    /**
     * The message protocol version.
     *
     * @var string
     */
    private string $protocolVersion;

    /**
     * AbstractMessage constructor.
     *
     * @param StreamInterface|null $body [optional] The message body, if any.
     * @param string $protocolVersion [optional] The message HTTP protocol
     * version. If the protocol version is not specified, it will
     * default to <code>'1.1'</code>.
     */
    public function __construct(
        StreamInterface $body = null,
        string $protocolVersion = '1.1',
    ) {
        $this->setBody($body);
        $this->setProtocolVersion($protocolVersion);
    }

    /**
     * Returns the body of the message.
     *
     * @return StreamInterface|null The message body.
     */
    #[Pure] public function getBody(): ?StreamInterface
    {
        return $this->body;
    }

    /**
     * Changes the body of the message.
     *
     * @param StreamInterface|null $body The message body.
     */
    private function setBody(?StreamInterface $body)
    {
        $this->body = $body;
    }

    /**
     * Returns the version of the message HTTP protocol.
     *
     * @return string The message HTTP protocol version.
     */
    #[Pure] public function getProtocolVersion(): string
    {
        return $this->protocolVersion;
    }

    /**
     * Changes the version of the message HTTP protocol.
     *
     * @param string $version The message HTTP protocol version.
     */
    protected function setProtocolVersion(string $version)
    {
        $this->protocolVersion = $version;
    }
}
