<?php

/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace HTTP\Request;

use HTTP\AbstractMessage;
use HTTP\File\UploadedFileInterface;
use HTTP\Request\Cookie\CookieInterface;
use HTTP\URL\UrlInterface;
use InvalidArgumentException;
use JetBrains\PhpStorm\Pure;

class Request extends AbstractMessage implements RequestInterface
{
    private string $method;

    private UrlInterface $url;

    private ?string $requestTarget;

    /**
     * @var array
     */
    private array $serverParams;

    /**
     * @var CookieInterface[]
     */
    private array $cookieParams;

    /**
     * @var array
     */
    private array $queryParams;

    /**
     * @var array
     */
    private array $attributes = [];

    /**
     * @var UploadedFileInterface[]
     */
    private array $uploadedFiles = [];

    /**
     * @var array|object|null
     */
    private array|null|object $parsedBody;

    /**
     * Request constructor.
     *
     * @param string $method The request method.
     * @param UrlInterface $url The request URI.
     * @param string|null $requestTarget
     * @param array $serverParams
     * @param array $cookieParams
     * @param array $queryParams
     */
    public function __construct(
        string $method,
        UrlInterface $url,
        ?string $requestTarget = null,
        array $serverParams = [],
        array $cookieParams = [],
        array $queryParams = []
    ) {
        parent::__construct();

        $this->setMethod($method);
        $this->setUrl($url);
        $this->setRequestTarget($requestTarget);

        $this->serverParams = $serverParams;
        $this->cookieParams = $cookieParams;
        $this->queryParams = $queryParams;
    }

    #[Pure] public function getRequestTarget(): ?string
    {
        if (!is_null($this->requestTarget)) {
            return $this->requestTarget;
        } elseif (!is_null($this->url)) {
            $requestTarget = empty(
                $this->url->getPath()
            ) ? '/' : $this->url->getPath(); // Add path

            $query = $this->url->getQuery();
            if (!empty($query)) {
                $requestTarget .= '?' . $query; // Add query
            }

            return $requestTarget;
        } else {
            return '/';
        }
    }

    /**
     * @inheritDoc
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Changes the method of the request.
     *
     * @param string $method The request method.
     */
    private function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): UrlInterface
    {
        return $this->url;
    }

    /**
     * @param UrlInterface $url
     */
    private function setUrl(UrlInterface $url)
    {
        $this->url = $url;

        $this->updateHost();
    }

    /**
     * @inheritDoc
     */
    public function getServerParams(): array
    {
        return $this->serverParams;
    }

    /**
     * @inheritDoc
     */
    public function getCookieParams(): array
    {
        return $this->cookieParams;
    }

    /**
     * @inheritDoc
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @inheritDoc
     */
    public function getUploadedFiles(): array
    {
        return $this->uploadedFiles;
    }

    /**
     * @inheritDoc
     */
    public function getParsedBody(): object|array|null
    {
        return $this->parsedBody;
    }

    /**
     * @inheritDoc
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @inheritDoc
     */
    public function getAttribute(string $name, string $default = null)
    {
        if (array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        } else {
            return $default;
        }
    }

    /**
     * @param string|null $requestTarget
     */
    private function setRequestTarget(?string $requestTarget)
    {
        $this->requestTarget = $requestTarget;
    }

    /**
     * Updates the Host header if it is not present.
     */
    private function updateHost(): void
    {
        $host = $this->url->getHost();

        if (!empty($host)) {
            if (!$this->getHeaderBag()->hasHeader('Host')) {
                $port = $this->url->getPort();

                if (!is_null($port)) {
                    $host .= ':' . $port;
                }

                $this->getHeaderBag()
                    ->addHeader('Host', $host, true);
            }
        }
    }
}
