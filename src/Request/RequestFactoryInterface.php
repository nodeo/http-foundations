<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Request;

/**
 * Interface RequestFactoryInterface
 * This interface contains various methods to create an instance of
 * {@link RequestInterface}.
 *
 * @package HTTP\Request
 */
interface RequestFactoryInterface
{

}
