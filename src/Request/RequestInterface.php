<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Request;

use HTTP\Cookie\ImmutableCookieBagInterface;
use HTTP\Header\ImmutableHeaderBagInterface;
use HTTP\MessageInterface;
use HTTP\URL\UrlInterface;
use JetBrains\PhpStorm\Pure;

/**
 * Interface RequestInterface
 *
 * This interface represents a request sent by the client to the server.
 *
 * A request is considered an immutable object.
 *
 * @package HTTP\Request
 */
interface RequestInterface extends MessageInterface
{
    /**
     * Returns the method of the request.
     *
     * @return string The request method.
     */
    public function getMethod(): string;

    /**
     * Returns the url of the request.
     *
     * @return UrlInterface The request url.
     */
    public function getUrl(): UrlInterface;

    /**
     * Returns the header bag of the request.
     *
     * @return ImmutableHeaderBagInterface The request header bag.
     */
    #[Pure] public function getHeaderBag(): ImmutableHeaderBagInterface;

    /**
     * Returns the cookie bag of the request.
     *
     * @return ImmutableCookieBagInterface The request cookie bag.
     */
    #[Pure] public function getCookieBag(): ImmutableCookieBagInterface;
}
