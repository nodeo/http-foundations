<?php

/*
 * Copyright (c) 2020, Juan (juan.valero@etu.univ-lyon1.fr), All rights reserved
 */

namespace HTTP\Request;

use HTTP\File\UploadedFileFactoryInterface;
use HTTP\URL\UrlFactoryInterface;
use IO\Stream\StreamFactory;

/**
 * Class RequestFactory
 * This class is the default implementation of {@link RequestFactoryInterface}.
 *
 * @package HTTP\Request
 */
class RequestFactory implements RequestFactoryInterface
{
    private UrlFactoryInterface $urlFactory;
    private StreamFactory $streamFactory;
    private UploadedFileFactoryInterface $uploadedFileFactory;

    /**
     * RequestFactory constructor.
     *
     * @param UrlFactoryInterface $urlFactory
     * @param StreamFactory $streamFactory
     * @param UploadedFileFactoryInterface $uploadedFileFactory
     */
    public function __construct(
        UrlFactoryInterface $urlFactory,
        StreamFactory $streamFactory,
        UploadedFileFactoryInterface $uploadedFileFactory
    ) {
        $this->urlFactory = $urlFactory;
        $this->streamFactory = $streamFactory;
        $this->uploadedFileFactory = $uploadedFileFactory;
    }

    /**
     * @inheritDoc
     */
    public function createRequestFromGlobals(): RequestInterface
    {
        $method = $_SERVER['REQUEST_METHOD'];

        $url = $this->urlFactory->createUrlFromGlobals();

        $uploadedFiles = [];

        foreach ($_FILES as $FILE) {
            $stream = $this->streamFactory->createStreamFromFile(
                $FILE['tmp_name']
            );
            $size = $FILE['size'];
            $error = $FILE['error'];
            $name = $FILE['name'] ?? null;
            $type = $FILE['type'] ?? null;

            $file = $this->uploadedFileFactory->createUploadedFile(
                $stream,
                $size,
                $error,
                $name,
                $type
            );

            $uploadedFiles[] = $file;
        }

        return new Request($method, $url, $_SERVER, $_COOKIE, $_GET);
    }

    /**
     * @inheritDoc
     */
    public function createRequest(
        string $method,
        string $url,
        array $serverParams = []
    ): RequestInterface {
        if (is_string($url)) {
            $url = $this->urlFactory->createUrl($url);
        }

        return new Request($method, $url, $serverParams);
    }
}
