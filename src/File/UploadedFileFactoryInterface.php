<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\File;

use IO\Stream\StreamInterface;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * Interface UploadedFileFactoryInterface
 *
 * This class contains various methods to create an instance of
 * {@link UploadedFileInterface}.
 *
 * @package HTTP\File
 */
interface UploadedFileFactoryInterface
{
    /**
     * Creates a new uploaded file with the specified values.
     *
     * @param StreamInterface $stream The stream associated with the file.
     * @param int|null $size [optional] The size of the file.
     * If no size is specified, it will be set to the stream size.
     * @param int $error [optional] The error associated with the file.
     * If no error is specified, it will be set to <code>UPLOAD_ERR_OK</code>
     * @param string|null $clientFilename [optional] The client file name.
     * @param string|null $clientMediaType [optional] The client media type.
     * @return UploadedFileInterface The created {@link UploadedFileInterface}
     * instance with the specified values.
     */
    public function createUploadedFile(
        StreamInterface $stream,
        ?int $size = null,
        #[ExpectedValues([
            UPLOAD_ERR_OK,
            UPLOAD_ERR_INI_SIZE,
            UPLOAD_ERR_FORM_SIZE,
            UPLOAD_ERR_PARTIAL,
            UPLOAD_ERR_NO_FILE,
            UPLOAD_ERR_NO_TMP_DIR,
            UPLOAD_ERR_CANT_WRITE,
            UPLOAD_ERR_EXTENSION
        ])] int $error = UPLOAD_ERR_OK,
        ?string $clientFilename = null,
        ?string $clientMediaType = null
    ): UploadedFileInterface;

    /**
     * Creates an array of uploaded files for the specified file name with
     * the contents of the $_FILES global.
     *
     * @param string $fileName The file name.
     * @return UploadedFileInterface[] An array of uploaded files for the
     * specified file name.
     */
    public function createUploadedFileFromGlobals(string $fileName): array;

    /**
     * Creates an array of uploaded files with the content of the $_FILES
     * global.
     *
     * @return UploadedFileInterface[][] An associative array where each key
     * is the file name and each value is the array of the uploaded
     * files for that name.
     */
    public function createUploadedFilesFromGlobals(): array;
}
