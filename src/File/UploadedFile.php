<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\File;

use InvalidArgumentException;
use IO\Stream\StreamFactory;
use IO\Stream\StreamInterface;
use JetBrains\PhpStorm\ExpectedValues;
use JetBrains\PhpStorm\Pure;
use RuntimeException;

/**
 * Class UploadedFile
 *
 * This class is the default implementation of {@link UploadedFileFactory}.
 *
 * @package IO\File
 */
class UploadedFile implements UploadedFileInterface
{
    private const UPLOAD_ERRORS = [
        UPLOAD_ERR_OK,
        UPLOAD_ERR_INI_SIZE,
        UPLOAD_ERR_FORM_SIZE,
        UPLOAD_ERR_PARTIAL,
        UPLOAD_ERR_NO_FILE,
        UPLOAD_ERR_NO_TMP_DIR,
        UPLOAD_ERR_CANT_WRITE,
        UPLOAD_ERR_EXTENSION
    ];

    /**
     * The stream associated with the file.
     *
     * @var StreamInterface
     */
    private StreamInterface $stream;

    /**
     * The size of the file.
     *
     * @var int|null
     */
    private ?int $size;

    /**
     * The error associated with the file.
     *
     * @var int
     */
    private int $error;

    /**
     * The name of the file.
     *
     * @var string|null
     */
    private ?string $clientFileName;

    /**
     * The media type of the file.
     *
     * @var string|null
     */
    private ?string $clientMediaType;

    /**
     * Defines whether the file has been moved or not.
     *
     * @var bool
     */
    private bool $moved;

    /**
     * UploadedFile constructor.
     *
     * @param StreamInterface $stream The stream associated with the file.
     * @param int|null $size [optional] The size of the file.
     * If no size is specified, it will be set to the stream size.
     * @param int $error [optional] The error associated with the uploaded file.
     * If no error is specified, it will be set to <code>UPLOAD_ERR_OK</code>
     * @param string|null $clientFilename [optional] The name of the file,
     * if any.
     * @param string|null $clientMediaType The media type of the file, if any.
     */
    public function __construct(
        StreamInterface $stream,
        ?int $size = null,
        #[ExpectedValues([
            UPLOAD_ERR_OK,
            UPLOAD_ERR_INI_SIZE,
            UPLOAD_ERR_FORM_SIZE,
            UPLOAD_ERR_PARTIAL,
            UPLOAD_ERR_NO_FILE,
            UPLOAD_ERR_NO_TMP_DIR,
            UPLOAD_ERR_CANT_WRITE,
            UPLOAD_ERR_EXTENSION
        ])] int $error = UPLOAD_ERR_OK,
        ?string $clientFilename = null,
        ?string $clientMediaType = null
    ) {
        $this->stream = $stream;

        if (is_null($size)) {
            $size = $this->getStream()->getSize();
        }

        $this->size = $size;

        if (!in_array($error, self::UPLOAD_ERRORS)) {
            throw new InvalidArgumentException(
                'The given error code is invalid'
            );
        }

        $this->setError($error);

        $this->setClientFilename($clientFilename);
        $this->setClientMediaType($clientMediaType);

        $this->moved = false;
    }

    /**
     * @inheritDoc
     */
    public function getStream(): StreamInterface
    {
        $this->validate();

        if (!isset($this->stream)) {
            throw new RuntimeException(
                'The stream associated with the file is not available'
            );
        }

        return $this->stream;
    }

    /**
     * @inheritDoc
     */
    public function moveTo(string $targetPath)
    {
        $this->validate();

        if (empty($targetPath)) {
            throw new InvalidArgumentException(
                'The given path is empty'
            );
        }

        $stream = $this->getStream();

        if ($stream->isSeekable()) {
            $stream->rewind();
        }

        $contents = $stream->getContents();

        $destination = (new StreamFactory())->createStreamFromFile(
            $targetPath,
            'w'
        );

        $destination->write($contents);

        $this->moved = true;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getError(): int
    {
        return $this->error;
    }

    /**
     * Changes the error associated with the file.
     *
     * @see https://www.php.net/manual/fr/features.file-upload.errors.php
     * @param int $error The error associated with the file.
     */
    private function setError(int $error)
    {
        if (!in_array($error, self::UPLOAD_ERRORS)) {
            throw new InvalidArgumentException(
                'The given error code is invalid'
            );
        }

        $this->error = $error;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getClientFilename(): ?string
    {
        return $this->clientFileName;
    }

    /**
     * Changes the name of the file.
     *
     * @param string|null $clientFilename The file name.
     */
    private function setClientFilename(?string $clientFilename)
    {
        $this->clientFileName = $clientFilename;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getClientMediaType(): ?string
    {
        return $this->clientMediaType;
    }

    /**
     * Changes the media type of the file.
     *
     * @param string|null $clientMediaType The file media type.
     */
    private function setClientMediaType(?string $clientMediaType)
    {
        $this->clientMediaType = $clientMediaType;
    }

    /**
     * Checks whether the file is valid.
     */
    private function validate()
    {
        if ($this->getError() !== UPLOAD_ERR_OK) {
            throw new RuntimeException(
                'An error has occurred while uploading the file.'
            );
        }

        if ($this->moved) {
            throw new RuntimeException('The file has already been moved');
        }
    }
}
