<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\File;

use InvalidArgumentException;
use IO\Stream\StreamFactory;
use IO\Stream\StreamInterface;
use JetBrains\PhpStorm\ExpectedValues;

use const UPLOAD_ERR_OK;

/**
 * Class UploadedFileFactory
 *
 * This class is the default implementation of
 * {@link UploadedFileFactoryInterface}.
 *
 * @package IO\File
 */
class UploadedFileFactory implements UploadedFileFactoryInterface
{
    private const FILE_FIELDS = ['name', 'type', 'size', 'tmp_name', 'error'];

    /**
     * @inheritDoc
     */
    public function createUploadedFile(
        StreamInterface $stream,
        ?int $size = null,
        #[ExpectedValues([
            UPLOAD_ERR_OK,
            UPLOAD_ERR_INI_SIZE,
            UPLOAD_ERR_FORM_SIZE,
            UPLOAD_ERR_PARTIAL,
            UPLOAD_ERR_NO_FILE,
            UPLOAD_ERR_NO_TMP_DIR,
            UPLOAD_ERR_CANT_WRITE,
            UPLOAD_ERR_EXTENSION
        ])] int $error = UPLOAD_ERR_OK,
        ?string $clientFilename = null,
        ?string $clientMediaType = null
    ): UploadedFileInterface {
        return new UploadedFile(
            $stream,
            $size,
            $error,
            $clientFilename,
            $clientMediaType
        );
    }

    /**
     * @inheritDoc
     */
    public function createUploadedFileFromGlobals(
        string $fileName
    ): array {
        if (!isset($_FILES[$fileName])) {
            throw new InvalidArgumentException(
                'No file with the specified file name has been uploaded'
            );
        }

        $file = $_FILES[$fileName];

        if (is_array($file['name'])) {
            $files = $this->normalizeFilesArray($file);
        } else {
            $files = [$file];
        }

        $uploadedFiles = [];

        $size = count($files);

        $streamFactory = new StreamFactory();

        for ($i = 0; $i < $size; $i++) {
            $file = $files[$i];

            $tmpName = $file['tmp_name'];
            $size = $file['size'] ?? null;
            $error = $file['error'] ?? UPLOAD_ERR_OK;
            $clientFileName = $file['name'] ?? null;
            $clientMediaType = $file['type'] ?? null;

            $stream = $streamFactory->createStreamFromFile($tmpName);

            $uploadedFiles = $this->createUploadedFile(
                $stream,
                $size,
                $error,
                $clientFileName,
                $clientMediaType
            );
        }

        return $uploadedFiles;
    }

    /**
     * @inheritDoc
     */
    public function createUploadedFilesFromGlobals(): array
    {
        $files = [];

        foreach ($_FILES as $FILE) {
            $fileName = $FILE['name'];

            $files[$fileName] = $this->createUploadedFileFromGlobals(
                $fileName
            );
        }

        return $files;
    }

    /**
     * Normalizes the specified files array.
     *
     * @see https://www.php.net/manual/fr/features.file-upload.post-method.php
     * @param array $files The files array.
     * @return string[][][] An associative array where each key is the file id
     * and each value is an associative array of the file properties.
     */
    private function normalizeFilesArray(array $files): array
    {
        $normalizedFilesArray = [];

        $size = count($files['name']);

        foreach (self::FILE_FIELDS as $field) {
            for ($i = 0; $i < $size; $i++) {
                $normalizedFilesArray[$i][$field] = $files[$field][$i];
            }
        }

        return $normalizedFilesArray;
    }
}
