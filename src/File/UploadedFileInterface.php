<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\File;

use IO\Stream\StreamInterface;
use JetBrains\PhpStorm\Pure;

/**
 * Interface UploadedFileInterface
 *
 * This interface represents a file uploaded through a HTTP request.
 *
 * @package IO\File
 */
interface UploadedFileInterface
{
    /**
     * Returns the stream associated with the file.
     *
     * @return StreamInterface The stream associated with the file.
     */
    public function getStream(): StreamInterface;

    /**
     * Move the file to a new location.
     *
     * @param string $targetPath The path to which to move the file.
     */
    public function moveTo(string $targetPath);

    /**
     * Returns the size of the file.
     *
     * @return int|null The file size or null if unknown.
     */
    #[Pure] public function getSize(): ?int;

    /**
     * Returns the error associated with the uploaded file.
     *
     * @see http://php.net/manual/en/features.file-upload.errors.php
     * @return int The error associated with the uploaded file. If the file was
     * uploaded successfully, it will return <code>UPLOAD_ERR_OK</code>.
     */
    #[Pure] public function getError(): int;

    /**
     * Returns the name of the file.
     *
     * @return string|null The file name or null if none was specified.
     */
    #[Pure] public function getClientFileName(): ?string;

    /**
     * Returns the media type of the file.
     *
     * @return string|null The file media type or null if none was specified.
     */
    #[Pure] public function getClientMediaType(): ?string;
}
