<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\URL\Query;

use InvalidArgumentException;
use JetBrains\PhpStorm\Pure;

/**
 * Class UrlQuery
 *
 * This class is the implementation of {@link UrlQueryInterface}.
 *
 * @package HTTP\URI\Query
 */
class UrlQuery implements UrlQueryInterface
{
    /**
     * @var array The query parameters.
     */
    private array $parameters;

    /**
     * UrlQuery constructor.
     *
     * @param string $query The query string.
     * An empty string for the query is equivalent to an empty query.
     */
    public function __construct(string $query)
    {
        $this->setQuery($query);
    }

    /**
     * Changes the query string.
     *
     * @param string $query The query string.
     */
    private function setQuery(string $query)
    {
        if (empty($query)) {
            $this->parameters = [];
        } else {
            parse_str($query, $parameters);

            foreach ($parameters as $name => $value) {
                $this->parameters[$name] = $value;
            }
        }
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function isEmpty(): bool
    {
        return empty($this->parameters);
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function hasParameter(string $parameter): bool
    {
        return array_key_exists($parameter, $this->parameters);
    }

    /**
     * @inheritDoc
     */
    public function getParameter(string $name): string
    {
        if (!$this->hasParameter($name)) {
            throw new InvalidArgumentException(
                'The query doesn\'t contain the specified parameter'
            );
        }

        return $this->parameters[$name];
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function equals(string $query): bool
    {
        return $query === $this->__toString();
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function __toString(): string
    {
        if (empty($this->parameters)) {
            return '';
        } else {
            return '?' . $this->getParametersLine();
        }
    }

    /**
     * Returns the parameters of the query as a string.
     *
     * @return string The query parameters as "key=value" format,
     * separated by the character '&'.
     */
    private function getParametersLine(): string
    {
        return http_build_query($this->parameters);
    }
}
