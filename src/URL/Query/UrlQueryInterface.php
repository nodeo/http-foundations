<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\URL\Query;

use JetBrains\PhpStorm\Pure;

/**
 * Interface UrlQueryInterface
 *
 * This interface represents the query component of the URI.
 * It is indicated by the first question mark "?" and ends with the start
 * of the fragment part.
 *
 * The query component pattern is not standardized but usually consists
 * of a list of key-value pairs separated by the character '&'.
 *
 * A query is considered an immutable object.
 *
 * @package HTTP\URI\Query
 */
interface UrlQueryInterface
{
    /**
     * Checks whether the query is empty.
     *
     * @return bool <code>true</code> if the query contains no parameters,
     * <code>false</code> otherwise.
     */
    #[Pure] public function isEmpty(): bool;

    /**
     * Returns the parameters of the query.
     *
     * @return string[] The query parameters as an associative array. Each key
     * stands for a parameter name and each value is the value for that
     * parameter.
     */
    #[Pure] public function getParameters(): array;

    /**
     * Checks whether the query contains the specified parameter
     *
     * @param string $parameter The parameter name.
     * @return bool <code>true</code> if the query contains the specified
     * parameter, <code>false</code> otherwise.
     */
    #[Pure] public function hasParameter(string $parameter): bool;

    /**
     * Returns the value associated to the specified parameter.
     *
     * @param string $name The parameter name.
     * @return string The value associated to the specified parameter.
     */
    public function getParameter(string $name): string;

    /**
     * Checks whether the query equals the specified query.
     *
     * @param string $query The query.
     * @return bool <code>true</code> if the query equals the specified
     * query, <code>false</code> otherwise.
     */
    #[Pure] public function equals(string $query): bool;

    /**
     * Returns the string representation of the query.
     *
     * @return string The query string representation, or an empty string if
     * the query is empty.
     */
    #[Pure] public function __toString(): string;
}
