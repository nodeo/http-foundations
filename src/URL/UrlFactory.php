<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\URL;

use HTTP\URL\Query\UrlQuery;
use InvalidArgumentException;

/**
 * Class UrlFactory
 *
 * This class is the default implementation of {@link UrlFactoryInterface}.
 *
 * @package HTTP\URL
 */
class UrlFactory implements UrlFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createUrl(string $url): UrlInterface
    {
        $url = utf8_encode($url);

        $components = parse_url($url);

        if ($components === false) {
            throw new InvalidArgumentException(
                'The specified URL is invalid'
            );
        }

        $scheme = $components['scheme'] ?? 'http';
        $user = $components['user'] ?? '';
        $password = $components['pass'] ?? '';
        $host = $components['host'] ?? 'localhost';
        $port = $components['port'] ?? null;
        $path = $components['path'] ?? '';
        $query = new UrlQuery($components['query'] ?? '');
        $fragment = $components['fragment'] ?? '';

        return new Url(
            $scheme,
            $user,
            $password,
            $host,
            $port,
            $path,
            $query,
            $fragment
        );
    }

    /**
     * @inheritDoc
     */
    public function createUrlFromGlobals(): UrlInterface
    {
        $scheme = isset($_SERVER['HTTPS']) ? 'https' : 'http';

        if (isset($_SERVER['HTTP_HOST'])) {
            [$host, $port] = explode($_SERVER['HTTP_HOST'], ':');
        } else {
            $host = $_SERVER['SERVER_NAME'];
            $port = $_SERVER['SERVER_PORT'];
        }

        ['path' => $path, 'query' => $queryString] = parse_url(
            $_SERVER['REQUEST_URI']
        );

        $query = new UrlQuery($queryString);

        return new Url(
            $scheme,
            '',
            '',
            $host,
            $port,
            $path,
            $query,
            ''
        );
    }
}
