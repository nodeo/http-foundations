<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\URL;

/**
 * Interface UrlFactoryInterface
 *
 * This interface contains various methods to create an instance of
 * {@link UrlInterface}.
 *
 * @package HTTP\URL
 */
interface UrlFactoryInterface
{
    /**
     * Creates an URL with the specified URL.
     *
     * @param string $url The URL.
     * @return UrlInterface The created {@link UrlInterface} instance
     * with the specified URL.
     */
    public function createUrl(string $url): UrlInterface;

    /**
     * Creates an URL with the contents of the $_SERVER global.
     *
     * @return UrlInterface The created {@link UrlInterface} instance
     * with the $_SERVER global contents.
     */
    public function createUrlFromGlobals(): UrlInterface;
}
