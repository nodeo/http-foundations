<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpArrayKeyDoesNotMatchArrayShapeInspection */

namespace HTTP\URL;

use HTTP\URL\Query\UrlQuery;
use HTTP\URL\Query\UrlQueryInterface;
use InvalidArgumentException;
use JetBrains\PhpStorm\Pure;

/**
 * Class Url
 *
 * This class is the default implementation of {@link UrlInterface}.
 *
 * @package HTTP\URL
 */
class Url implements UrlInterface
{
    private const PORT_LOWER_LIMIT = 0;
    private const PORT_UPPER_LIMIT = 65535;

    private const SCHEME_STANDARD_PORTS = [
        'http' => 80,
        'https' => 443
    ];

    /**
     * The URL scheme.
     *
     * @var string
     */
    private string $scheme;

    /**
     * The URL user name.
     *
     * @var string
     */
    private string $user;

    /**
     * The URL password.
     *
     * @var string
     * @deprecated Use of the format "user:password" in the userinfo field is
     * deprecated.
     */
    private string $password;

    /**
     * The URL host.
     *
     * @var string
     */
    private string $host;

    /**
     * The URL port.
     *
     * @var int|null
     */
    private ?int $port;

    /**
     * The URL path.
     *
     * @var string
     */
    private string $path;

    /**
     * The URL query.
     *
     * @var UrlQuery
     */
    private UrlQueryInterface $query;

    /**
     * The URL fragment.
     *
     * @var string
     */
    private string $fragment;

    /**
     * Url constructor.
     *
     * @param string $scheme The URL scheme.
     * @param string $user The URL user.
     * @param string $password The URL password.
     * @param string $host The URL host.
     * @param int $port The URL port.
     * @param string $path The URL path.
     * @param UrlQueryInterface $query The URL query.
     * @param string $fragment The URL fragment.
     */
    public function __construct(
        string $scheme,
        string $user,
        string $password,
        string $host,
        int $port,
        string $path,
        UrlQueryInterface $query,
        string $fragment
    ) {
        $this->setScheme($scheme);
        $this->setUser($user);
        $this->setPassword($password);
        $this->setHost($host);
        $this->setPort($port);
        $this->setPath($path);
        $this->setQuery($query);
        $this->setFragment($fragment);
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getScheme(): string
    {
        return $this->scheme;
    }

    /**
     * Sets the scheme component of the URL.
     *
     * @see https://tools.ietf.org/html/rfc3986#section-3.1
     * @param string $scheme The URL scheme.
     */
    private function setScheme(string $scheme)
    {
        $this->scheme = $this->normalize($scheme); // Scheme should be
        // normalized as specified by the RFC 3986

        if (!array_key_exists($scheme, self::SCHEME_STANDARD_PORTS)) {
            throw new InvalidArgumentException(
                'The specified scheme is invalid'
            );
        }
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getAuthority(): string
    {
        $host = $this->getHost();

        if (empty($host)) {
            return '';
        }

        $authority = '';

        $userInfo = $this->getUserInformation();
        if (!empty($userInfo)) {
            $authority .= $userInfo . '@';
        }

        $authority .= $host;

        $port = $this->getPort();

        if (!is_null($port)) {
            $authority .= ':' . $port;
        }

        return $authority;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getUserInformation(): string
    {
        $user = $this->getUser();

        if (empty($user)) {
            return '';
        }

        $userInfo = $user;

        $password = $this->getPassword();

        if (!is_null($password)) {
            $userInfo .= ':' . $password;
        }

        return $userInfo;
    }

    /**
     * Returns the user name of the URL.
     *
     * @return string The URL user name, or an empty string if no user name is
     * present.
     */
    #[Pure] private function getUser(): string
    {
        return $this->user;
    }

    /**
     * Sets the user name of the URL.
     *
     * @param string $user The URL user name.
     */
    private function setUser(string $user)
    {
        $this->user = $user;
    }

    /**
     * Returns the password of the URL.
     *
     * @return string The URL password, or an empty string if no password is
     * present.
     * @deprecated Use of the format "user:password" in the userinfo field is
     * deprecated.
     */
    #[Pure] private function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Change the password of the URL.
     *
     * @param string $password The URL password.
     * @deprecated Use of the format "user:password" in the userinfo field is
     * deprecated.
     */
    private function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getHost(): string
    {
        return $this->host;
    }

    /**
     * Sets the host component of the URL.
     *
     * @param string $host The URL host.
     */
    private function setHost(string $host)
    {
        $this->host = $this->normalize($host);
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getPort(): ?int
    {
        return $this->port;
    }

    /**
     * Sets the port component of the URL.
     *
     * @param int|null $port The URL port.
     */
    private function setPort(?int $port)
    {
        if (is_int($port)) {
            if (
                $port < self::PORT_LOWER_LIMIT
                || $port > self::PORT_UPPER_LIMIT
            ) {
                throw new InvalidArgumentException(
                    sprintf(
                        'The given port is outside bounds.' .
                        ' It must be between %d and %d',
                        self::PORT_LOWER_LIMIT,
                        self::PORT_UPPER_LIMIT
                    )
                );
            }
        }

        $standardPort = self::SCHEME_STANDARD_PORTS[$this->getScheme()];

        if ($port === $standardPort) {
            // Standard port for the scheme
            $this->port = null;
        } else {
            $this->port = $port;
        }
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Sets the path component of the URL.
     *
     * @param string $path The URL path.
     */
    private function setPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getQuery(): UrlQueryInterface
    {
        return $this->query;
    }

    /**
     * Sets the query component of the URL.
     *
     * @param string $query The URL query.
     */
    private function setQuery(string $query)
    {
        $this->query = new UrlQuery($query);
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getFragment(): string
    {
        return $this->fragment;
    }

    /**
     * Sets the fragment component of the URL.
     *
     * @param string $fragment The URL fragment.
     */
    private function setFragment(string $fragment)
    {
        $this->fragment = $fragment;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function __toString(): string
    {
        $uri = '';

        $scheme = $this->getScheme();

        if (!empty($scheme)) {
            $uri .= $scheme . ':';
        }

        $authority = $this->getAuthority();

        if (!empty($authority)) {
            $uri .= '//' . $authority;
        }

        $path = $this->getPath();

        if (!empty($path)) {
            if ($path[0] === '/') {
                // Absolute path
                if (empty($authority)) {
                    $path = ltrim(
                        $path,
                        '/'
                    ); // Reduce starting slashes to one
                }
            } elseif (!empty($authority)) {
                $path = '/' . $path; // Prefix the path by '/'
            }
        }

        $uri .= $path;

        $query = $this->getQuery();

        if (!$query->isEmpty()) {
            $uri .= $query->__toString();
        }

        $fragment = $this->getFragment();

        if (!empty($fragment)) {
            $uri .= '#' . $fragment;
        }

        return $uri;
    }

    /**
     * Normalizes the specified component.
     *
     * @param string $component The component.
     * @return string The normalized component.
     */
    #[Pure] private function normalize(string $component): string
    {
        return strtolower($component);
    }
}
