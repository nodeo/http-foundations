<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Header;

use JetBrains\PhpStorm\Pure;

/**
 * Interface HeaderInterface
 *
 * This class represents a header field as specified by the RFC 7230.
 *
 * @link https://datatracker.ietf.org/doc/html/rfc7230#section-3.2
 * @package HTTP\Header
 */
interface HeaderInterface
{
    /**
     * Returns the name of the header.
     *
     * @return string The header name.
     */
    public function getName(): string;

    /**
     * Returns the value of the header.
     *
     * @return string[] A string array of the header values.
     */
    public function getValue(): array;

    /**
     * Add a new value to the header.
     *
     * @param string $value The value.
     */
    public function addValue(string $value);

    /**
     * Returns the values of the header as a string.
     *
     * @return string The header values separated by a comma, or an empty
     * string if the header doesn't contain any value.
     */
    #[Pure] public function getHeaderLine(): string;

    /**
     * Returns the string representation of the header.
     *
     * @return string The header string representation as "name: values" format.
     */
    #[Pure] public function __toString(): string;
}
