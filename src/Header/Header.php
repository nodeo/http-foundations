<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Header;

use JetBrains\PhpStorm\Pure;

/**
 * Class Header
 *
 * This class is the default implementation of {@link HeaderInterface}.
 *
 * @package HTTP\Header
 */
class Header implements HeaderInterface
{
    /**
     * The header name.
     *
     * @var string
     */
    private string $name;

    /**
     * The header values as a string array.
     *
     * @var string[]
     */
    private array $values;

    /**
     * Header constructor.
     *
     * @param string $name The header name.
     * @param string[] $values The header values, as a string array.
     */
    public function __construct(string $name, array $values)
    {
        $this->name = $name;
        $this->values = $values;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getValue(): array
    {
        return $this->values;
    }

    /**
     * @param string $value
     */
    public function addValue(string $value)
    {
        if (!in_array($value, $this->getValue())) {
            $this->values[] = $value;
        }
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getHeaderLine(): string
    {
        return implode(',', $this->getValue());
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function __toString(): string
    {
        return $this->getName() . ':' . $this->getValue();
    }
}
