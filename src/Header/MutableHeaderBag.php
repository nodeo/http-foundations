<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Header;

use InvalidArgumentException;

/**
 * Class MutableCookieBag
 *
 * This class is the default implementation of
 * {@link MutableHeaderBagInterface}.
 *
 * @package HTTP\Header
 */
class MutableHeaderBag extends ImmutableHeaderBag implements MutableHeaderBagInterface
{
    /**
     * @inheritDoc
     */
    public function addHeader(
        string $name,
        array|string $value,
        bool $first = false
    ) {
        if ($this->hasHeader($name)) {
            throw new InvalidArgumentException(
                'The specified header exists already'
            );
        }

        $this->setHeader($name, $value, $first);
    }

    /**
     * @inheritDoc
     */
    public function removeHeader(string $name)
    {
        if (!$this->hasHeader($name)) {
            throw new InvalidArgumentException(
                'The specified header doesn\'t exist'
            );
        }

        $headerName = $this->getHeaderName($name);

        unset($this->headers[$headerName]);

        $normalizedName = $this->normalizeHeaderName($name);
        unset($this->headerNames[$normalizedName]);
    }
}
