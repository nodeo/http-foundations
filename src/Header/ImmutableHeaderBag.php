<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Header;

use InvalidArgumentException;
use JetBrains\PhpStorm\Pure;

/**
 * Class ImmutableHeaderBag
 *
 * This class is the default implementation of {@link ImmutableHeaderBagInterface}.
 *
 * @package HTTP\Header
 */
class ImmutableHeaderBag implements ImmutableHeaderBagInterface
{
    /**
     * The headers list.
     *
     * @var Header[]
     */
    protected array $headers;

    /**
     * The headers names as a string array.
     * Each key stands for the normalized header name and each value is its
     * actual name, as specified by the user.
     *
     * @var string[]
     */
    protected array $headerNames;

    /**
     * Cursor position.
     *
     * @var int
     */
    private int $position;

    /**
     * ImmutableHeaderBag constructor.
     *
     * @param array $headers The headers list.
     */
    public function __construct(array $headers)
    {
        $this->setHeaders($headers);

        $this->rewind();
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function hasHeader(string $name): bool
    {
        $normalizedName = $this->normalizeHeaderName($name);

        return array_key_exists($normalizedName, $this->headerNames);
    }

    /**
     * @inheritDoc
     */
    public function getHeader(string $name): HeaderInterface
    {
        if ($this->hasHeader($name)) {
            throw new InvalidArgumentException(
                'The specified header doesn\'t exist'
            );
        }

        $name = $this->getHeaderName($name);

        return $this->headers[$name];
    }

    /**
     * Changes the headers.
     *
     * @param array $headers The headers.
     */
    private function setHeaders(array $headers)
    {
        $this->headers = [];
        $this->headerNames = [];

        foreach ($headers as $header => $value) {
            $this->setHeader($header, $value);
        }
    }

    /**
     * Creates a new header with the specified value if it doesn't exist, or
     * changes his value if it already exists.
     *
     * @param string $name The header name.
     * @param array|string $value The header value.
     * @param bool $first [optional] Defines whether the header should be placed
     * on the top of the headers. By default, it will be placed at the end.
     */
    protected function setHeader(
        string $name,
        array|string $value,
        bool $first = false
    ) {
        $this->validateHeaderName($name);
        $headerName = $this->getHeaderName($name);

        $normalizedHeaderValue = $this->validateAndNormalizeHeaderValue($value);
        $normalizedHeader = new Header($name, $normalizedHeaderValue);

        if ($first) {
            // Place it at the beginning of the headers array
            $this->headers =
                [$headerName => $normalizedHeader] + $this->headers;
        } else {
            $this->headers[$headerName] = $normalizedHeader;
        }

        $normalizedHeaderName = $this->normalizeHeaderName($headerName);
        $this->headerNames[$normalizedHeaderName] = $headerName;
    }

    /**
     * Normalizes the specified header name.
     *
     * @param string $name The header name.
     * @return string The normalized header name.
     */
    protected function normalizeHeaderName(string $name): string
    {
        return strtolower($name);
    }

    /**
     * Asserts that the specified header name is valid.
     *
     * @param mixed $name The header name.
     */
    private function validateHeaderName(mixed $name)
    {
        if (!is_string($name)) {
            throw new InvalidArgumentException(
                'Header name must be compatible with RFC 7230'
            );
        }
    }

    /**
     * Asserts that the specified header value is valid.
     *
     * @param mixed $value The header value.
     */
    private function validateHeaderValue(mixed $value)
    {
        if (!is_string($value)) {
            throw new InvalidArgumentException(
                'Invalid header value. It must be compatible with RFC 7230'
            );
        }
    }

    /**
     * Asserts that the specified header value is valid and normalizes it.
     *
     * @param string[]|string $value The header value.
     * @return array The normalized header value.
     */
    private function validateAndNormalizeHeaderValue(array|string $value): array
    {
        if (is_array($value)) {
            if (empty($value)) {
                throw new InvalidArgumentException(
                    'Header value must be a non empty array'
                );
            }
        } else {
            $value = [$value];
        }

        $normalizedValues = [];

        foreach ($value as $v) {
            $this->validateHeaderValue($v);

            $normalizedValues[] = trim($v);
        }

        return $normalizedValues;
    }

    /**
     * Returns the actual header name, as specified by the user, associated to
     * the specified non normalized header name.
     *
     * @param string $name The non normalized header name.
     * @return string The actual header name, as specified by the user,
     * associated to the specified non normalized name, or the name itself if
     * the header doesn't exist.
     */
    #[Pure] protected function getHeaderName(string $name): string
    {
        $normalizedName = $this->normalizeHeaderName($name);

        return $this->hasHeader($name) ? $this->headerNames[$normalizedName] :
            $name;
    }

    /**
     * @inheritDoc
     */
    public function current(): Header
    {
        return $this->headers[$this->key()];
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        $this->position++;
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return array_keys($this->headerNames)[$this->position];
    }

    /**
     * @inheritDoc
     */
    public function valid(): bool
    {
        return isset(array_keys($this->headerNames)[$this->position]);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->position = 0;
    }
}
