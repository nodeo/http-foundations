<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Header;

use HTTP\AbstractMessage;
use Iterator;
use JetBrains\PhpStorm\Pure;

/**
 * Interface ImmutableHeaderBagInterface
 *
 * This interface represents the {@link HeaderInterface} list of an
 * {@link AbstractMessage}.
 *
 * By default, a {@link ImmutableHeaderBagInterface} is considered an immutable object
 * contrary to the {@link MutableHeaderBagInterface} which can be modified.
 *
 * @see HeaderInterface
 * @package HTTP\Header
 */
interface ImmutableHeaderBagInterface extends Iterator
{
    /**
     * Returns the bag headers.
     *
     * @return Header[] An associative array of headers where each key is the
     * header name and each value is the header associated to this name.
     */
    #[Pure] public function getHeaders(): array;

    /**
     * Checks if the bag contains the specified header.
     *
     * @param string $name The header case insensitive name.
     * @return bool <code>true</code> if the bag contains the specified header
     * <code>false</code> otherwise.
     */
    #[Pure] public function hasHeader(string $name): bool;

    /**
     * Returns the header associated to the specified name.
     *
     * @param string $name The header case insensitive name.
     * @return HeaderInterface The header associated to the specified name.
     */
    public function getHeader(string $name): HeaderInterface;
}
