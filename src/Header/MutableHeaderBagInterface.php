<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Header;

/**
 * Interface MutableHeaderBagInterface
 *
 * This interface represents a {@link ImmutableHeaderBagInterface} that can be modified.
 *
 * @package HTTP\Header
 */
interface MutableHeaderBagInterface extends ImmutableHeaderBagInterface
{
    /**
     * Add a new header to the bag.
     *
     * @param string $name The header name.
     * @param array|string $value The header value.
     * @param bool $first [optional] Defines whether the header should be placed
     * on the top of the headers. By default, it will be placed at the end.
     */
    public function addHeader(
        string $name,
        array|string $value,
        bool $first = false
    );

    /**
     * Removes the specified header from the bag.
     *
     * @param string $name The header case insensitive name.
     */
    public function removeHeader(string $name);
}
