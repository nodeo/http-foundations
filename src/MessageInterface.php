<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP;

use IO\Stream\StreamInterface;
use JetBrains\PhpStorm\Pure;

/**
 * Interface MessageInterface
 *
 * This interface represents a message sent through the HTTP protocol.
 * It can be sent from the client to the server ({@link RequestInterface})
 * or vice versa ({@link ResponseInterface}).
 *
 * A message is considered an immutable object.
 *
 * Please report to the RFC 7230 specification for further details about
 * messages.
 *
 * @see https://tools.ietf.org/html/rfc7230
 * @package HTTP
 */
interface MessageInterface
{
    /**
     * Returns the body of the message.
     *
     * @return StreamInterface|null The message body, or null if the body is
     * empty.
     */
    #[Pure] public function getBody(): ?StreamInterface;

    /**
     * Returns the version of the message HTTP protocol.
     *
     * @return string The message HTTP protocol version.
     */
    #[Pure] public function getProtocolVersion(): string;
}
