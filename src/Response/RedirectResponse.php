<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Response;

use HTTP\URL\UrlInterface;
use InvalidArgumentException;
use IO\Stream\StreamInterface;
use JetBrains\PhpStorm\ExpectedValues;

/**
 * Class RedirectResponse
 *
 * This class represents a response that leads to a redirect. The user
 * may also specify a countdown after which the client will be effectively
 * redirected.
 *
 * @package HTTP\Response
 */
class RedirectResponse extends Response
{
    /**
     * RedirectResponse constructor.
     *
     * @param UrlInterface $requestedUri The requested URI.
     * @param int|null $redirectCountdown [optional] The time in seconds
     * after which the user is effectively redirected.
     * If the redirection countdown is not specified, the user will directly
     * be redirect.
     * @param int $statusCode [optional] The response status code.
     * If the status code is not specified, it will default to
     * <code>Response::HTTP_OK</code>.
     * @param string|null $reasonPhrase [optional] The response reason phrase.
     * If the reason phrase is not specified or is null, it will default to
     * the recommended reason phrase for the response status code.
     * @param StreamInterface|null $body [optional] The response body, if any.
     * @param string $protocolVersion [optional] The response protocol version.
     * If the protocol version is not specified, it will default to
     * <code>'1.1'</code>.
     * @param array $headers [optional] The response headers, if any.
     */
    public function __construct(
        UrlInterface $requestedUri,
        int $redirectCountdown = null,
        #[ExpectedValues(valuesFromClass: ResponseInterface::class)]
        int $statusCode = self::HTTP_OK,
        string $reasonPhrase = null,
        StreamInterface $body = null,
        string $protocolVersion = '1.1',
        array $headers = []
    ) {
        if ($redirectCountdown < 0) {
            throw new InvalidArgumentException('Invalid countdown');
        }

        parent::__construct(
            $statusCode,
            $reasonPhrase,
            $body,
            $protocolVersion,
            $headers
        );

        $this->getHeaderBag()->addHeader('Location', $requestedUri);

        if (!is_null($redirectCountdown)) {
            $this->getHeaderBag()
                ->addHeader(
                    'Refresh',
                    sprintf(
                        '%d;url=%s',
                        $redirectCountdown,
                        $requestedUri
                    )
                );
        }
    }
}
