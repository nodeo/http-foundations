<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Response;

use HTTP\AbstractMessage;
use HTTP\Cookie\MutableCookieBag;
use HTTP\Cookie\MutableCookieBagInterface;
use HTTP\Header\MutableHeaderBag;
use HTTP\Header\MutableHeaderBagInterface;
use InvalidArgumentException;
use IO\Stream\StreamInterface;
use JetBrains\PhpStorm\ExpectedValues;
use JetBrains\PhpStorm\Pure;

/**
 * Class Response
 *
 * This class is the default implementation of {@link ResponseInterface}.
 *
 * @package HTTP\Response
 */
class Response extends AbstractMessage implements ResponseInterface
{
    /**
     * The default reason phrases.
     *
     * @var array
     */
    private const DEFAULT_STATUS_PHRASES = [
        self::HTTP_CONTINUE => 'Continue',
        self::HTTP_SWITCHING_PROTOCOLS => 'Switching Protocols',
        self::HTTP_PROCESSING => 'Processing',
        self::HTTP_EARLY_HINTS => 'Early Hints',
        self::HTTP_OK => 'OK',
        self::HTTP_CREATED => 'Created',
        self::HTTP_ACCEPTED => 'Accepted',
        self::HTTP_NON_AUTHORITATIVE_INFORMATION => 'Non-Authoritative Information',
        self::HTTP_NO_CONTENT => 'No Content',
        self::HTTP_RESET_CONTENT => 'Reset Content',
        self::HTTP_PARTIAL_CONTENT => 'Partial Content',
        self::HTTP_MULTI_STATUS => 'Multi-Status',
        self::HTTP_ALREADY_REPORTED => 'Already Reported',
        self::HTTP_IM_USED => 'IM used',
        self::HTTP_MULTIPLE_CHOICES => 'Multiple Choices',
        self::HTTP_MOVED_PERMANENTLY => 'Moved Permanently',
        self::HTTP_FOUND => 'Found',
        self::HTTP_SEE_OTHER => 'See Other',
        self::HTTP_NOT_MODIFIED => 'Not Modified',
        self::HTTP_USE_PROXY => 'Use Proxy',
        self::HTTP_UNUSED => '(Unused)',
        self::HTTP_TEMPORARY_REDIRECT => 'Temporary Redirect',
        self::HTTP_PERMANENT_REDIRECT => 'Permanent Redirect',
        self::HTTP_BAD_REQUEST => 'Bad Request',
        self::HTTP_UNAUTHORIZED => 'Unauthorized',
        self::HTTP_PAYMENT_REQUIRED => 'Payment Required',
        self::HTTP_FORBIDDEN => 'Forbidden',
        self::HTTP_NOT_FOUND => 'Not Found',
        self::HTTP_METHOD_NOT_ALLOWED => 'Method Not Allowed',
        self::HTTP_NOT_ACCEPTABLE => 'Not Acceptable',
        self::HTTP_PROXY_AUTHENTICATION_REQUIRED => 'Proxy Authentication Required',
        self::HTTP_REQUEST_TIMEOUT => 'Request Timeout',
        self::HTTP_CONFLICT => 'Conflict',
        self::HTTP_GONE => 'Gone',
        self::HTTP_LENGTH_REQUIRED => 'Length Required',
        self::HTTP_PRECONDITION_FAILED => 'Precondition Failed',
        self::HTTP_PAYLOAD_TOO_LARGE => 'Payload Too Large',
        self::HTTP_URI_TOO_LONG => 'URI Too long',
        self::HTTP_UNSUPPORTED_MEDIA_TYPE => 'Unsupported Media Type',
        self::HTTP_RANGE_NOT_SATISFIABLE => 'Range Not Satisfiable',
        self::HTTP_EXPECTATION_FAILED => 'Expectation Failed',
        self::HTTP_I_AM_A_TEAPOT => 'I\'m a teapot',
        self::HTTP_MISDIRECTED_REQUEST => 'Misdirected Request',
        self::HTTP_UNPROCESSABLE_ENTITY => 'Unprocessable Entity',
        self::HTTP_LOCKED => 'Locked',
        self::HTTP_FAILED_DEPENDENCY => 'Failed Dependency',
        self::HTTP_TOO_EARLY => 'Too Early',
        self::HTTP_UPGRADE_REQUIRED => 'Upgrade Required',
        self::HTTP_PRECONDITION_REQUIRED => 'Precondition Required',
        self::HTTP_TOO_MANY_REQUESTS => 'Too Many Requests',
        self::HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE => 'Request Header Fields Too Large',
        self::HTTP_UNAVAILABLE_FOR_LEGAL_REASONS => 'Unavailable For Legal Reasons',
        self::HTTP_INTERNAL_SERVER_ERROR => 'Internal Server Error',
        self::HTTP_NOT_IMPLEMENTED => 'Not Implemented',
        self::HTTP_BAD_GATEWAY => 'Bad Gateway',
        self::HTTP_SERVICE_UNAVAILABLE => 'Service Unavailable',
        self::HTTP_GATEWAY_TIMEOUT => 'Gateway Timeout',
        self::HTTP_VERSION_NOT_SUPPORTED => 'HTTP Version Not Supported',
        self::HTTP_VARIANT_ALSO_NEGOTIATES => 'Variant Also Negotiates',
        self::HTTP_INSUFFICIENT_STORAGE => 'Insufficient Storage',
        self::HTTP_LOOP_DETECTED => 'Loop Detected',
        self::HTTP_NOT_EXTENDED => 'Not Extended',
        self::HTTP_NETWORK_AUTHENTICATION_REQUIRED => 'Network Authentication Required'
    ];

    /**
     * The response header bag.
     *
     * @var MutableHeaderBagInterface
     */
    private MutableHeaderBagInterface $headerBag;

    /**
     * The response cookie bag.
     *
     * @var MutableCookieBagInterface
     */
    private MutableCookieBagInterface $cookieBag;

    /**
     * The response status code.
     *
     * @var int
     */
    private int $statusCode;

    /**
     * The response reason phrase.
     *
     * @var string
     */
    private string $reasonPhrase;

    /**
     * Response constructor.
     *
     * @param int $statusCode [optional] The response status code.
     * If the status code is not specified, it will default to
     * <code>Response::HTTP_OK</code>.
     * @param string|null $reasonPhrase [optional] The response reason phrase.
     * If the reason phrase is not specified or is null, it will default to
     * the recommended reason phrase for the response status code.
     * @param StreamInterface|null $body [optional] The response body, if any.
     * @param string $protocolVersion [optional] The response protocol version.
     * If the protocol version is not specified, it will default to
     * <code>'1.1'</code>.
     * @param array $headers [optional] The response headers, if any.
     */
    public function __construct(
        #[ExpectedValues(valuesFromClass: ResponseInterface::class)]
        int $statusCode = self::HTTP_OK,
        string $reasonPhrase = null,
        StreamInterface $body = null,
        string $protocolVersion = '1.1',
        array $headers = []
    ) {
        parent::__construct($body, $protocolVersion);

        $this->headerBag = new MutableHeaderBag($headers);
        $this->cookieBag = new MutableCookieBag();

        $this->setStatus($statusCode, $reasonPhrase);
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getHeaderBag(): MutableHeaderBagInterface
    {
        return $this->headerBag;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getCookieBag(): MutableCookieBagInterface
    {
        return $this->cookieBag;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * Changes the status code of the response.
     *
     * @see http://tools.ietf.org/html/rfc7231#section-6
     * @param int $code The response status code.
     */
    private function setStatusCode(
        #[ExpectedValues(valuesFromClass: ResponseInterface::class)] int $code
    ) {
        if (!array_key_exists($code, self::DEFAULT_STATUS_PHRASES)) {
            throw new InvalidArgumentException(
                'The specified status code is invalid'
            );
        }

        $this->statusCode = $code;
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function getReasonPhrase(): string
    {
        return $this->reasonPhrase;
    }

    /**
     * Changes the reason phrase of the response.
     *
     * @param string|null $reasonPhrase [response] The response reason phrase.
     * If the reason phrase is not specified, is null or is an empty string, it
     * will default to the recommended reason phrase for the response status
     * code.
     */
    private function setReasonPhrase(?string $reasonPhrase = null)
    {
        if (is_null($reasonPhrase)) {
            $this->reasonPhrase = self::DEFAULT_STATUS_PHRASES[$this->statusCode];
        } else {
            $this->reasonPhrase = $reasonPhrase;
        }
    }

    /**
     * Changes the status of the response.
     *
     * @param int $code The response status code.
     * @param string|null $reasonPhrase The response reason phrase.
     * If the reason phrase is not specified, is null or is an empty string, if
     * will default to the recommended reason phrase for the response status
     * code.
     */
    private function setStatus(
        #[ExpectedValues(valuesFromClass: ResponseInterface::class)] int $code,
        ?string $reasonPhrase = null
    ) {
        $this->setStatusCode($code);
        $this->setReasonPhrase($reasonPhrase);
    }

    /**
     * @inheritDoc
     */
    #[Pure] public function send()
    {
        foreach ($this->getHeaderBag() as $header) {
            header($header->__toString());
        }
    }
}
