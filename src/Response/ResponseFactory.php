<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Response;

/**
 * Class ResponseFactory
 *
 * This class is the default implementation of {@link ResponseFactoryInterface}.
 *
 * @package HTTP\Response
 */
class ResponseFactory implements ResponseFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createResponse(
        int $code = ResponseInterface::HTTP_OK,
        string $reasonPhrase = null
    ): ResponseInterface {
        return new Response($code, $reasonPhrase);
    }
}
