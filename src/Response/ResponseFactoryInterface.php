<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Response;

/**
 * Class ResponseFactory
 *
 * This interface contains various methods to create an instance of
 * {@link Response} instance.
 *
 * @package HTTP\Response
 */
interface ResponseFactoryInterface
{
    /**
     * Creates a Response with the specified status.
     *
     * @param int $code [optional] The response status code.
     * If the status code is not specified, it will default to
     * <code>Response::HTTP_OK</code>.
     * @param string|null $reasonPhrase [optional] The response reason phrase.
     * If the reason phrase is not specified or is null, it will default to
     * the recommended reason phrase for the response status code.
     * @return ResponseInterface The created {@link ResponseInterface} instance
     * with the specified status.
     */
    public function createResponse(
        int $code = ResponseInterface::HTTP_OK,
        string $reasonPhrase = null
    ): ResponseInterface;
}
