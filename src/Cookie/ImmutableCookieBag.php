<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Cookie;

/**
 * Class ImmutableCookieBag
 *
 * This class is the default implementation of {@link ImmutableCookieBagInterface}.
 *
 * @package HTTP\Cookie
 */
class ImmutableCookieBag implements ImmutableCookieBagInterface
{
    public function getCookies(): array
    {
        // TODO: Implement getCookies() method.
    }

    public function getCookie(string $name): CookieInterface
    {
        // TODO: Implement getCookie() method.
    }
}
