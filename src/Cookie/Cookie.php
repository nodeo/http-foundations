<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Cookie;

/**
 * Class Cookie
 *
 * This class is the default implementation of {@link CookieInterface}.
 *
 * @package HTTP\Cookie
 */
class Cookie implements CookieInterface
{

}
