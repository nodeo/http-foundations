<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Cookie;

use HTTP\AbstractMessage;
use JetBrains\PhpStorm\Pure;

/**
 * Interface ImmutableCookieBagInterface
 *
 * This class represents the {@link CookieInterface} list of an
 * {@link AbstractMessage}.
 *
 * @package HTTP\Cookie
 */
interface ImmutableCookieBagInterface
{
    /**
     * Returns the bag cookies.
     *
     * @return CookieInterface[] An associative array of cookies where each
     * key is the cookie name and each value is the cookie associated to that
     * name.
     */
    #[Pure] public function getCookies(): array;

    /**
     * Returns the cookie associated to the specified name.
     *
     * @param string $name The cookie name.
     * @return CookieInterface The cookie associated to the specified name.
     */
    #[Pure] public function getCookie(string $name): CookieInterface;
}
