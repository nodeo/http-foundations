<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Cookie;

/**
 * Class MutableCookieBag
 *
 * This class is the default implementation of
 * {@link MutableCookieBagInterface}.
 *
 * @package HTTP\Cookie
 */
class MutableCookieBag extends ImmutableCookieBag implements
    MutableCookieBagInterface
{

}
