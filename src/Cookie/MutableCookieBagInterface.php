<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Cookie;

/**
 * Interface MutableCookieBagInterface
 *
 * This interface represents a {@link ImmutableCookieBagInterface} that can be modified.
 *
 * @package HTTP\Cookie
 */
interface MutableCookieBagInterface extends ImmutableCookieBagInterface
{

}
