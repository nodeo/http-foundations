<?php

/*
 * Copyright (c) Nodeo - All Rights Reserved.
 * Unauthorized copying of this file, via any medium, is strictly prohibited.
 */

namespace HTTP\Cookie;

use JetBrains\PhpStorm\Pure;

/**
 * Interface CookieInterface
 *
 * This interface represents a HTTP cookie as specified by the RFC 6265.
 *
 * @link https://datatracker.ietf.org/doc/html/rfc6265
 * @package HTTP\Cookie
 */
interface CookieInterface
{
    /**
     * Returns the name of the cookie.
     *
     * @return string The cookie name.
     */
    #[Pure] public function getName(): string;

    /**
     * Returns the value of the cookie.
     *
     * @return string The cookie value.
     */
    #[Pure] public function getValue(): string;

    /**
     * Returns the timestamp after which the cookie will be destroyed.
     *
     * @return int The cookie expiration timestamp.
     */
    #[Pure] public function getExpirationTimestamp(): int;
}
